import{Task} from './Task'

export const TASKS: Task[] = [
    {
        id: 1,
        text: 'TestTask1',
        day: 'May 3th at 2:50pm',
        reminder: true,
    },
    {
        id: 2,
        text: 'TestTask2',
        day: 'May 4th at 2:40pm',
        reminder: true,
    },
    {
        id: 3,
        text: 'TestTask3',
        day: 'May 6th at 2:25pm',
        reminder: false,
    },

];